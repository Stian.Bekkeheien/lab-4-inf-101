package no.uib.inf101.gridview;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Color;

public class CellPositionToPixelConverter {
  //Feltvariabler
  Rectangle2D box;
  GridDimension gd;
  Double margin;

  //Constructor
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  //getBoundsForCell method
  public Rectangle2D getBoundsForCell(CellPosition cp){
    //cellHeight
    double heightMargins = gd.rows() + 1;
    double cellHeight = (box.getHeight() - (heightMargins * margin)) / gd.rows();
    //cellWidth
    double widthMargins = gd.cols() + 1;
    double cellWidth = (box.getWidth() - (margin * widthMargins)) / gd.cols();

    //x
    int spacesX = cp.row() + 1;
    double cellX = box.getX() + (cellWidth * cp.col()) + (margin * (cp.col() + 1));

    //y
    int spacesY = cp.col() + 1;
    double cellY = box.getY() + (cellHeight * cp.row()) + (margin * (cp.row() + 1));

    Rectangle2D returnitem = new Rectangle2D.Double(cellX, cellY, cellWidth,cellHeight);

    return returnitem;
      
  }
}
