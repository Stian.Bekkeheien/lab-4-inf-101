package no.uib.inf101.gridview;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Color;

public class GridView extends JPanel{
  private IColorGrid cg;
  private static final double OUTERMARGIN =  30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid){
    this.setPreferredSize(new Dimension(400, 300));
    this.cg = colorGrid;
  }
  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);


    // // Tegner først et fyllt rosa kvadrat
    // Rectangle2D shape1 = new Rectangle2D.Double(100, 20, 50, 50);
    // g2.setColor(Color.PINK);
    // g2.fill(shape1);

    // // Tegner deretter omrisset av et svart rektangel.
    // // Ny tegning kommer «oppå» det som er tegnet fra før.
    // Rectangle2D shape2 = new Rectangle2D.Double(110, 30, 50, 30);
    // g2.setColor(Color.BLACK);
    // g2.draw(shape2);
  }

  private void drawGrid(Graphics2D g2) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;

    double width = this.getWidth() - (2 * x);
    double height = this.getHeight() - (2 * x);
    Rectangle2D rec = new Rectangle2D.Double(x, y, width, height);
    g2.setColor(MARGINCOLOR);
    g2.fill(rec);

    CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(rec, cg, OUTERMARGIN);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection collection, CellPositionToPixelConverter converter){
  
    for (CellColor c : collection.getCells()){
      Rectangle2D rec = converter.getBoundsForCell(c.cellPosition());
      if(c.color() == null){
        g2.setColor(MARGINCOLOR);
      }
      else {
        g2.setColor(c.color());
      }
      g2.fill(rec);
    }
  }
  
}