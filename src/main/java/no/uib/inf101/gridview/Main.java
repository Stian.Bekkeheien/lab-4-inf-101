package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

public class Main {
  public static void main(String[] args, Object Color) {
    ColorGrid colorGrid = new ColorGrid(3, 4);
    JFrame frame = new JFrame();
    
    colorGrid.set(new CellPosition(0, 0), new Color(255,0,0));
    colorGrid.set(new CellPosition(0, 3), new Color(0,0,255));
    colorGrid.set(new CellPosition(2, 0), new Color(255,255,0));
    colorGrid.set(new CellPosition(2, 3), new Color(0,255,0));
    GridView view = new GridView(colorGrid);

    frame.setTitle("INF202");
    frame.setContentPane(view);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

  }
}
