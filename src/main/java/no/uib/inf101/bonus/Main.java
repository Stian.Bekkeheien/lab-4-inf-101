package no.uib.inf101.bonus;

import java.awt.Color;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

public class Main {
  public static void main(String[] args, Object Color) {
    ColorGrid colorGrid = new ColorGrid(3, 4);
    
    colorGrid.set(new CellPosition(0, 0), new Color(255,0,0));
    colorGrid.set(new CellPosition(0, 3), new Color(0,0,255));
    colorGrid.set(new CellPosition(2, 0), new Color(255,255,0));
    colorGrid.set(new CellPosition(2, 3), new Color(0,255,0));

  }
}
