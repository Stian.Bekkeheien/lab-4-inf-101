package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{
  private int rows;
  private int columns;
  private Color color;
  

  private List<List<Color>> grid;
  public ColorGrid(int rows, int columns){
    this.rows = rows;
    this.columns = columns;
    this.grid = new ArrayList<>();
    for (int i = 0; i < rows; i++){
      List<Color> rowlist = new ArrayList<>();
      for (int j = 0; j < columns; j++){
        rowlist.add(j, null);
      }
      grid.add(rowlist);
    }



  }
  @Override
  public int rows() {
    return rows;
  }
  @Override
  public int cols() {
    return columns;
  }
  @Override
  public List<CellColor> getCells() {
    List<CellColor> returnList = new ArrayList<>();
    for (int i = 0; i < grid.size(); i++){
      List<Color> row = grid.get(i);
      for (Color color : row){
        CellPosition pos = new CellPosition(i, row.indexOf(color));
        CellColor element = new CellColor(pos, color);
        returnList.add(element);

      }

    }
    return returnList;
  }
  @Override
  public Color get(CellPosition pos) {
    return grid.get(pos.row()).get(pos.col());
  }
  @Override
  public void set(CellPosition pos, Color color) {
    grid.get(pos.row()).remove(pos.col());
    grid.get(pos.row()).add(pos.col(), color);
  }
}
